/// <reference path="./typings/qualtrics.d.ts"/>
/// <reference path="./typings/handlebars.d.ts"/>

type Config = {
  apiKey: string;
  calendarId: string;
  lastDate: Date;
  daysBack: number;
  label: string;
  defaultEvents: string[];
  partnerList: string;
};
interface Window {
  FollowBackConfig: Config;
}
