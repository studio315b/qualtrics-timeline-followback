import { CheckedFlag } from "./Types";

type DayCell = {
  events: { text: string; style: string }[];
  title: number;
  id: string;
  background: string;
  showTitle: boolean;
  active: boolean;
  count: number;
  label: string;
};

export const outOfMonthCell: DayCell = {
  showTitle: false,
  active: false,
  background: "darkgrey",
  events: [],
  title: 0,
  id: "",
  label: "",
  count: 0
};

export const inactiveCell = (title: number): DayCell => ({
  showTitle: true,
  title,
  active: false,
  background: "lightgrey",
  events: [],
  id: "",
  count: 0,
  label: ""
});

export default DayCell;
