export default interface IQuestionScope {
  getEarliestDay(): Date;
  getLatestDay(): Date;
}
