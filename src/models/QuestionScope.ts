import IQuestionScope from "./IQuestionScope";

export default class QuestionScope implements IQuestionScope {
  private _start: Date;
  private _end: Date;
  public constructor(lastDate: Date | string, daysBack: number) {
    this._end = new Date(lastDate);
    this._end.setHours(0, 0, 0, 0);
    this._start = new Date(this._end);
    this._start.setDate(this._end.getDate() - daysBack);
  }
  getEarliestDay = () => new Date(this._start);
  getLatestDay = () => new Date(this._end);
}
