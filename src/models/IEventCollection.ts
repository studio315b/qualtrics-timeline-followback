import { EventId } from "./Types";
import EventInstance from "./EventInstance";

export default interface IEventCollection {
  createEvent(): EventId;
  getEventsForDay(day: string): EventInstance[];
  getUserEvents(): (EventInstance & { id: EventId })[];
  updateEvent(id: EventId, data: Partial<EventInstance>): void;
  deleteEvent(id: EventId): void;
  cloneEvent(id: EventId): void;
}
