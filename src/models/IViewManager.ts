import IController from "./IController";
import View from "./View";

export default interface IViewManager {
  addViewController(controller: IController): void;
  setActiveView(view: View): void;
  getActiveView(): View;
}
