import { OutputObject } from "./Types";

export default interface IDateCollection {
  setDate(key: string, value: number);
  getDates(): { [key: string]: number };
  getDate(date: string): number;
  getCount(): number;
}
