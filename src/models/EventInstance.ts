type EventInstance = {
  day: string;
  name: string;
  user: boolean;
};

export default EventInstance;
