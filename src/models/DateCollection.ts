import IDateCollection from "./IDateCollection";

export default class DateCollection implements IDateCollection {
  private _dates: { [key: string]: number } = {};

  public setDate = (key: string, value: number) =>
    value == 0
      ? this._dates[key]
        ? delete this._dates[key]
        : null
      : (this._dates[key] = value);

  public getDate = (date: string) => this._dates[date] || 0;

  public getDates = () => this._dates;

  public getCount = () =>
    Object.keys(this._dates).length > 0
      ? Object.keys(this._dates)
          .sort()
          .map(date => this._dates[date])
          .reduce((p, c) => p + c)
      : 0;
}
