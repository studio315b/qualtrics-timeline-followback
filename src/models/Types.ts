export type EventId = number;
export type HiddenFlag = "hidden" | undefined;
export type CheckedFlag = "checked" | undefined;
export type DisabledFlag = "disabled" | undefined;

export type OutputObject = {
  day: string;
  events: string[];
  instance: number;
  total: number;
};
