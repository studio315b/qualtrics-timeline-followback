import IViewManager from "./IViewManager";
import IController from "./IController";
import View from "./View";

export default class ViewManager implements IViewManager {
  private _controllers: IController[] = [];
  private _activeView: View;

  public constructor(activeView: View) {
    this._activeView = activeView;
  }

  addViewController = (controller: IController) =>
    this._controllers.push(controller);

  setActiveView = (view: View) => (
    (this._activeView = view), this._controllers.forEach(c => c.refresh())
  );

  getActiveView = () => this._activeView;
}
