import { getEventsFromCalendarItem } from "./../utils/EventUtils";
import { GoogleCalendarResponse } from "./GoogleCalendarTypes";
import { EventId } from "./Types";
import EventInstance from "./EventInstance";
import IEventCollection from "./IEventCollection";
import IQuestionScope from "./IQuestionScope";
import { dayAfter } from "../utils/DateUtils";

const emptyEvent: EventInstance = { day: "", name: "", user: true };

export default class EventCollection implements IEventCollection {
  private _nextEvent = 0;
  private _userEvents: EventInstance[] = [];
  private _publicEvents: EventInstance[] = [];

  public constructor(
    apiKey: string,
    calendarKey: string,
    questionScope: IQuestionScope,
    defaultEvents: string[]
  ) {
    this.updatePublicEvents(apiKey, calendarKey, questionScope);
    defaultEvents.forEach(name =>
      this.updateEvent(this.createEvent(), { name })
    );
  }

  // API
  public createEvent = () => {
    const id = this._nextEvent++;
    this._userEvents[id] = emptyEvent;
    return id;
  };

  private userEventsAsArray = (): EventInstance[] =>
    Object.entries(this._userEvents).map(([_, e]) => e);

  public getEventsForDay = (day: string) =>
    [...this._publicEvents, ...this.userEventsAsArray()].filter(
      e => e.day == day && e.name != ""
    );

  public getUserEvents = () =>
    Object.entries(this._userEvents).map(([id, e]) => ({
      ...e,
      id: parseInt(id)
    }));

  public updateEvent = (id: EventId, data: Partial<EventInstance>) =>
    (this._userEvents[id] = { ...this._userEvents[id], ...data });

  public deleteEvent = (id: EventId) => delete this._userEvents[id];

  public cloneEvent = (id: EventId) =>
    this._userEvents.splice(id, 0, this._userEvents[id]);

  // Helpers

  private generateQuery = (
    apiKey: string,
    calendarKey: string,
    startDate: string,
    endDate: string
  ) =>
    encodeURI(
      `https://www.googleapis.com/calendar/v3/calendars/${calendarKey}/events?singleEvents=true&timeMax=${endDate}&timeMin=${startDate}&key=${apiKey}`
    );

  private updatePublicEvents = (
    apiKey: string,
    calendarKey: string,
    questionScope: IQuestionScope
  ) => {
    const startDate = questionScope.getEarliestDay().toISOString();
    const endDate = dayAfter(questionScope.getLatestDay()).toISOString();
    jQuery.ajax({
      type: "GET",
      url: this.generateQuery(apiKey, calendarKey, startDate, endDate),
      dataType: "json",
      success: (r: GoogleCalendarResponse) =>
        (this._publicEvents = r.items
          .map(i => getEventsFromCalendarItem(i))
          .reduce((c, p) => c.concat(p))),
      error: r => {
        console.error(r);
      }
    });
  };
}
