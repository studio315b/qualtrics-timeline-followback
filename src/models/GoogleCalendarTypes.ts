export type GoogleCalendarTime = { date?: string; dateTime?: string };

export type GoogleCalendarItem = {
  start: GoogleCalendarTime;
  end: GoogleCalendarTime;
  summary: string;
};

export type GoogleCalendarResponse = {
  items: GoogleCalendarItem[];
};
