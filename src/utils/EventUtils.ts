import {
  GoogleCalendarItem,
  GoogleCalendarTime
} from "../models/GoogleCalendarTypes";
import EventInstance from "../models/EventInstance";
import { formatted, dayAfter, formattedUTC } from "./DateUtils";

const isAllDay = (item: GoogleCalendarItem) => item.start.date && item.end.date;

const asDateObject = (time: GoogleCalendarTime) =>
  time.date
    ? new Date(time.date)
    : time.dateTime
    ? new Date(time.dateTime)
    : (() => {
        throw new Error(`Unparsable: ${time}`);
      })();

export const getEventsFromCalendarItem = (
  item: GoogleCalendarItem
): EventInstance[] => {
  const start = asDateObject(item.start);
  const end = asDateObject(item.end);
  const name = item.summary;
  if (isAllDay(item)) {
    const events: EventInstance[] = [];
    let counter = start;
    while (counter < end) {
      events.push({ name, day: formattedUTC(counter), user: false });
      counter = dayAfter(counter);
    }
    return events;
  } else {
    return [{ name, day: formatted(start), user: false }];
  }
};
