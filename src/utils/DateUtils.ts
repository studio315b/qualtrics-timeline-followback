export const firstOf = (date: Date) => {
  const first = new Date(date);
  first.setDate(1);
  return first;
};

export const lastOf = (date: Date) => {
  const last = new Date(date);
  last.setMonth(last.getMonth() + 1);
  last.setDate(0);
  return last;
};

export const sundayOf = (date: Date) => {
  const sunday = new Date(date);
  sunday.setDate(date.getDate() - date.getDay());
  return sunday;
};

export const saturdayOf = (date: Date) => {
  const sunday = new Date(date);
  sunday.setDate(date.getDate() + (6 - date.getDay()));
  return sunday;
};

export const dayAfter = (date: Date) => {
  const tommorrow = new Date(date);
  tommorrow.setDate(date.getDate() + 1);
  return tommorrow;
};

export const formatted = (date: Date) => {
  const dayValue = date.getDate();
  const day = dayValue < 10 ? `0${dayValue}` : `${dayValue}`;

  // Months are stored 0 indexed, so +1 to get the human number
  const monthValue = date.getMonth() + 1;
  const month = monthValue < 10 ? `0${monthValue}` : `${monthValue}`;

  const year = date.getFullYear();
  return `${year}-${month}-${day}`;
};

export const formattedUTC = (date: Date) => {
  const dayValue = date.getUTCDate();
  const day = dayValue < 10 ? `0${dayValue}` : `${dayValue}`;

  // Months are stored 0 indexed, so +1 to get the human number
  const monthValue = date.getUTCMonth() + 1;
  const month = monthValue < 10 ? `0${monthValue}` : `${monthValue}`;

  const year = date.getUTCFullYear();
  return `${year}-${month}-${day}`;
};
