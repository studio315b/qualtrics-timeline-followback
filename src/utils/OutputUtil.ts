import DateCollection from "../models/DateCollection";
import EventCollection from "../models/EventCollection";

type innerBlock = { day: string; instance: number; total: number };

const createArray = (
  length: number,
  day: string,
  total: number,
  arr: innerBlock[]
): innerBlock[] =>
  length == 0
    ? arr
    : createArray(length - 1, day, total, [
        { day, total, instance: length },
        ...arr
      ]);

export const generateOutput = (
  dates: DateCollection,
  events: EventCollection
) =>
  JSON.stringify(
    Object.keys(dates.getDates())
      .map(c => createArray(dates.getDate(c), c, dates.getDate(c), []))
      .reduce((p, c) => p.concat(c))
      .map(d => ({
        ...d,
        events: events.getEventsForDay(d.day).map(e => e.name)
      }))
      .sort((a, b) => a.day.localeCompare(b.day))
  );
