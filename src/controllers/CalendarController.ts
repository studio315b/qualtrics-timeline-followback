import DayCell, { outOfMonthCell, inactiveCell } from "../models/DayCell";
import BaseController from "./BaseController";
import IEventCollection from "../models/IEventCollection";
import IQuestionScope from "../models/IQuestionScope";
import {
  firstOf,
  formatted,
  lastOf,
  sundayOf,
  saturdayOf,
  dayAfter
} from "../utils/DateUtils";
import IViewManager from "../models/IViewManager";
import { HiddenFlag, DisabledFlag } from "../models/Types";
import View from "../models/View";
import IDateCollection from "../models/IDateCollection";

type Context = {
  hidden: HiddenFlag;
  month: string;
  weeks: DayCell[][];
  nextDisabled: DisabledFlag;
  previousDisabled: DisabledFlag;
};

export default class CalendarController extends BaseController<Context> {
  private _eventCollection: IEventCollection;
  private _dateCollection: IDateCollection;
  private _questionScope: IQuestionScope;
  private _viewManager: IViewManager;
  private _activeMonth: Date;
  private _label: string;
  private _onDone: () => void;

  public constructor(
    div: JQuery<HTMLElement>,
    view: RenderTemplate,
    label: string,
    onDone: () => void,
    eventCollection: IEventCollection,
    dateCollection: IDateCollection,
    questionScope: IQuestionScope,
    viewManager: IViewManager
  ) {
    super(div, view);
    this._label = label;
    this._onDone = onDone;
    this._eventCollection = eventCollection;
    this._dateCollection = dateCollection;
    this._questionScope = questionScope;
    this._activeMonth = firstOf(questionScope.getLatestDay());
    this._viewManager = viewManager;
    this.refresh();
  }

  // Actions
  private updateDateActionFactory = (key: string) => (e: JQuery.ChangeEvent) =>
    this._dateCollection.setDate(key, parseInt(e.target.value));

  private previousMonthAction = () => (
    this._activeMonth.setMonth(this._activeMonth.getMonth() - 1), this.refresh()
  );

  private nextMonthAction = () => (
    this._activeMonth.setMonth(this._activeMonth.getMonth() + 1), this.refresh()
  );

  private toEventsAction = () =>
    this._viewManager.setActiveView(View.EventsView);

  // Controller
  protected bind = (context: Context) => {
    context.weeks.forEach(week =>
      week.forEach(day => {
        if (day.active) {
          const cb = this.find(`#Count${day.id}`);
          cb.change(this.updateDateActionFactory(day.id));
        }
      })
    );
    const previousButton = this.find("#Previous");
    previousButton.click(this.previousMonthAction);
    const nextButton = this.find("#Next");
    nextButton.click(this.nextMonthAction);
    const eventsButton = this.find("#ToEvents");
    eventsButton.click(this.toEventsAction);
    const doneButton = this.find("#Done");
    doneButton.click(this._onDone);
  };

  protected getContext = (): Context => ({
    hidden:
      this._viewManager.getActiveView() != View.CalendarView
        ? "hidden"
        : undefined,
    month:
      this._activeMonth.toLocaleString("en-us", { month: "long" }) +
      " " +
      this._activeMonth.getFullYear(),
    weeks: this.generateWeeks(),
    nextDisabled:
      lastOf(this._activeMonth) > this._questionScope.getLatestDay()
        ? "disabled"
        : undefined,
    previousDisabled:
      firstOf(this._activeMonth) < this._questionScope.getEarliestDay()
        ? "disabled"
        : undefined
  });

  // Utils
  private generateWeeks = () => {
    let active = sundayOf(firstOf(this._activeMonth));
    const month = [];
    while (active < saturdayOf(lastOf(this._activeMonth))) {
      const week = [];
      for (var i = 0; i < 7; i++) {
        week.push(this.createCell(active));
        active = dayAfter(active);
      }
      month.push(week);
    }
    return month;
  };

  private createCell = (day: Date): DayCell => {
    if (day.getMonth() != this._activeMonth.getMonth()) {
      return outOfMonthCell;
    }
    let title = day.getDate();
    if (
      day > this._questionScope.getLatestDay() ||
      day < this._questionScope.getEarliestDay()
    ) {
      return inactiveCell(title);
    }
    const id = formatted(day);
    const events = this._eventCollection
      .getEventsForDay(formatted(day))
      .map(e => ({
        text: e.name,
        style: e.user ? "font-weight: bold" : "font-style: italic"
      }));
    const count = this._dateCollection.getDate(formatted(day));
    return {
      showTitle: true,
      active: true,
      background: events.length > 0 ? "pink" : "white",
      label: this._label,
      events,
      title,
      id,
      count
    };
  };
}
