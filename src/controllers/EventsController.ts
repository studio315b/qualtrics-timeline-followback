import { HiddenFlag, EventId } from "./../models/Types";
import BaseController from "./BaseController";
import IEventCollection from "../models/IEventCollection";
import EventInstance from "../models/EventInstance";
import IViewManager from "../models/IViewManager";
import View from "../models/View";
import IQuestionScope from "../models/IQuestionScope";

type Context = {
  hidden: HiddenFlag;
  events: (EventInstance & { id: EventId })[];
};

export default class EventsController extends BaseController<Context> {
  private readonly _eventCollection: IEventCollection;
  private readonly _questionScope: IQuestionScope;
  private readonly _viewManager: IViewManager;

  constructor(
    div: JQuery,
    view: RenderTemplate,
    eventCollection: IEventCollection,
    questionScope: IQuestionScope,
    viewManager: IViewManager
  ) {
    super(div, view);
    this._eventCollection = eventCollection;
    this._viewManager = viewManager;
    this._questionScope = questionScope;
    this.refresh();
  }

  // Actions

  private addEventAction = () => (
    this._eventCollection.createEvent(), this.refresh()
  );

  private deleteEventActionFactory = (id: number) => () => (
    this._eventCollection.deleteEvent(id), this.refresh()
  );

  private cloneEventActionFactory = (id: number) => () => (
    console.log("Cloning"), this._eventCollection.cloneEvent(id), this.refresh()
  )

  private updateEventActionFactory = (id: number, mode: "day" | "name") => (
    e: JQuery.ChangeEvent
  ) =>
    this._eventCollection.updateEvent(id, {
      [mode]: (e.target as HTMLInputElement).value
    });

  private doneAction = () => this._viewManager.setActiveView(View.CalendarView);

  // Controller
  public getContext = (): Context => ({
    hidden:
      this._viewManager.getActiveView() != View.EventsView
        ? "hidden"
        : undefined,
    events: this._eventCollection.getUserEvents()
  });

  protected bind = (context: Context) => {
    context.events.forEach(({ id }) => {
      const removeButton = this.find(`#Remove${id}`);
      const cloneButton = this.find(`#Clone${id}`);
      const datePicker = this.find(`#Date${id}`);
      const nameTextBox = this.find(`#Name${id}`);
      removeButton.click(this.deleteEventActionFactory(id));
      cloneButton.click(this.cloneEventActionFactory(id));
      datePicker.datepicker({
        uiLibrary: "bootstrap4",
        iconsLibrary: "fontawesome",
        maxDate: this._questionScope.getLatestDay(),
        minDate: this._questionScope.getEarliestDay(),
        format: "yyyy-mm-dd"
      });
      datePicker.change(this.updateEventActionFactory(id, "day"));
      nameTextBox.change(this.updateEventActionFactory(id, "name"));
    });

    // add event
    const addEventButton = this.find("#AddEvent");
    addEventButton.click(this.addEventAction);

    // next
    const doneButton = this.find("#ToCalendar");
    doneButton.click(this.doneAction);
  };
}
