import IController from "../models/IController";

export default abstract class BaseController<Context> implements IController {
  protected draw: (context: Context) => void;
  protected abstract bind(context?: Context): void;
  protected abstract getContext(): Context;
  public refresh = () => {
    const context = this.getContext();
    this.draw(context);
    this.bind(context);
  };
  protected find: <T extends HTMLElement>(query: string) => JQuery<T>;

  constructor(div: JQuery, view: RenderTemplate) {
    this.draw = (context: Context) => div.html(view(context));
    this.find = <T extends HTMLElement>(query: string) =>
      div.find(query) as JQuery<T>;
  }
}
