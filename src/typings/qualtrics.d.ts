interface SurveyEngine {
  addOnload: (action: () => void) => void;
  addOnReady: (action: () => void) => void;
  addOnUnload: (action: () => void) => void;
  setEmbeddedData: (id: string, value: string) => void;
}

interface QualtricsSystem {
  SurveyEngine: SurveyEngine;
}

declare interface QuestionObject {
  getChoiceContainer(): HTMLElement;
  setChoiceValue: (id: string, value: string) => void;
  clickNextButton: () => void;
  hideNextButton: () => void;
  questionId: string;
}

declare const Qualtrics: QualtricsSystem;
