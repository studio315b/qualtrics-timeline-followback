declare module "*.hbs" {
  const render: RenderTemplate;
  export = render;
}

type RenderTemplate = (context?: any) => string;
