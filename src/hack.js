// One of out dependencies needs jquery at $ but that's not available in Qualtrics, so this file sets $ equal to the qualtrics provided jQuery
// It has to be in Js rather than Ts because Ts wants to enforce const.
$ = jQuery;
