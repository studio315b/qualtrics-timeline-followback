import { generateOutput } from "./utils/OutputUtil";
import "gijgo/js/gijgo.min";

import "./hack";
import CalendarController from "./controllers/CalendarController";
import DateCollection from "./models/DateCollection";
import EventCollection from "./models/EventCollection";
import EventsController from "./controllers/EventsController";
import QuestionScope from "./models/QuestionScope";
import View from "./models/View";
import ViewManager from "./models/ViewManager";
import qualtricsView = require("./views/qualtrics.hbs");
import calendarView = require("./views/calendarView.hbs");
import eventsView = require("./views/eventsView.hbs");
const {
  apiKey,
  calendarId,
  label,
  lastDate,
  daysBack,
  defaultEvents,
  partnerList
} = window.FollowBackConfig;

// Initialize Models
const viewManager = new ViewManager(View.EventsView);
const questionScope = new QuestionScope(lastDate, daysBack);
const eventCollection = new EventCollection(
  apiKey,
  calendarId,
  questionScope,
  defaultEvents
);
const dateCollection = new DateCollection();

Qualtrics.SurveyEngine.addOnload(function () {
  const that: QuestionObject = this;
  const view = document.createElement("div");
  view.innerHTML = qualtricsView();
  const input = that.getChoiceContainer().lastElementChild as HTMLInputElement;
  input.style.display = "none";
  that.hideNextButton();
  that.getChoiceContainer().appendChild(view);

  const onDone = () => {
    if (dateCollection.getCount() >= partnerList.split(",").length) {

      input.value = dateCollection.getCount().toString();
      Qualtrics.SurveyEngine.setEmbeddedData(
        "Events",
        `" + ${JSON.stringify(generateOutput(dateCollection, eventCollection))} + "`
      );
      that.clickNextButton();
    }
  };

  // Initialize View/Controller pairs
  viewManager.addViewController(
    new EventsController(
      jQuery("#EventsView"),
      eventsView,
      eventCollection,
      questionScope,
      viewManager
    )
  );
  viewManager.addViewController(
    new CalendarController(
      $("#CalendarView"),
      calendarView,
      label,
      onDone,
      eventCollection,
      dateCollection,
      questionScope,
      viewManager
    )
  );
});
