# Timeline Followback Utility for Qualtrics

A javascript micro-app to produce a simple Followback calendar inside a qualtrics survey that can be fed into a "Loop & Merge."

Features include:

- User defined events.
- Researcher defined events provided through a public Google calendar.
- Input verification and validation.

## Requirements

- (Node.js)[https://nodejs.org/en/]
- (Yarn)[https://yarnpkg.com/lang/en/]

## Building

To build the calendar ui, run `yarn build`. All other files are pure JS and can be used as-is