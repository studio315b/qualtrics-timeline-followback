import * as webpack from "webpack";

const config: webpack.Configuration = {
  mode: "production",
  entry: {
    followback: "./src/FollowBack.ts"
  },
  output: {
    filename: "[name].js",
    path: __dirname + "/dist"
  },
  resolve: {
    extensions: [".ts", ".js", ".hbs"]
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: "ts-loader" },
      { test: /\.hbs$/, loader: "handlebars-loader" }
    ]
  }
};

export default config;
