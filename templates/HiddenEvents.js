const events = JSON.parse("${e://Field/Events}");
const index = "${lm://CurrentLoopNumber}";
const event = events[index - 1];

Qualtrics.SurveyEngine.addOnload(function () {
  const realAnswer = document.getElementById("QR~" + this.questionId);
  realAnswer.value = event.events.join("\n");
  this.getQuestionContainer().style.display = "none";
});
