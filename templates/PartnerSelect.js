const partners = "${e://Field/Partners}";
const events = JSON.parse("${e://Field/Events}");
const index = "${lm://CurrentLoopNumber}";
const event = events[index - 1];

function getIterative(num) {
  switch (num % 10) {
    case 1:
      return num % 100 == 11 ? num + "th" : num + "st";
    case 2:
      return num % 100 == 12 ? num + "th" : num + "nd";
    case 3:
      return num % 100 == 13 ? num + "th" : num + "rd";
    default:
      return num + "th";
  }
}

Qualtrics.SurveyEngine.addOnload(function () {
  // update question
  document.getElementById("time").innerHTML = getIterative(event.instance);
  document.getElementById("date").innerHTML = new Date(event.day + " 1:00").toLocaleDateString();
  if(event.events.length != 0)
  {
    document.getElementById("events").innerHTML =
      "<ul>" + event.events.map(e => "<li>" + e + "</li>") + "</ul>";
  }
  else {
    document.getElementById("events").innerHTML = "";
  }


  // answer data
  const realAnswer = document.getElementById("QR~" + this.questionId);
  realAnswer.style.display = "none";
  const parent = realAnswer.parentElement;
  const select = document.createElement("select");
  select.style.minWidth = "25%";
  select.multiple = true;
  select.addEventListener("change", function () {
    const options = select.options;
    const selected = [];
    for (var i = 0; i < options.length; i++) {
      if (options[i].selected) {
        selected.push(options[i].value);
      }
    }
    realAnswer.value = selected.join(",");
  });

  partners.split(",").forEach(partner => {
    const partnerOption = document.createElement("option");
    partnerOption.value = partner;
    partnerOption.text = partner;
    select.appendChild(partnerOption);
  });
  parent.appendChild(select);
});
