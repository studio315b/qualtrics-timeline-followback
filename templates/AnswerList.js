const answerType = "Partners";
const questionId = "QID47";
const previousAnswers = "${e://Field/Partners}" != "" ? "${e://Field/Partners}".split(",") : [];
const index = "${lm://CurrentLoopNumber}";

Qualtrics.SurveyEngine.addOnload(function () {
  const answerList = document.getElementById("Answers");
  if (previousAnswers.length == 0) {
    this.getQuestionContainer().style.display = "none";
    return;
  }
  const answers = previousAnswers;
  answers.forEach(answerName => {
    const answer = document.createElement("li");
    answer.innerHTML = answerName;
    answerList.appendChild(answer);
  });
});

Qualtrics.SurveyEngine.addOnPageSubmit(function (type) {
  const answer = document.getElementById("QR~" + index + "_" + questionId);
  if (type == "next") {
    previousAnswers.push(answer.value)
    Qualtrics.SurveyEngine.setEmbeddedData(
      answerType,
      previousAnswers.join(",")
    );
  }
});
